<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://webw.us
 * @since      1.0.0
 *
 * @package    Testing
 * @subpackage Testing/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Testing
 * @subpackage Testing/includes
 * @author     We Build Websites LLC  <sales@webw.me>
 */
class Testing_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
